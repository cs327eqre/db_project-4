--drop view on_campus;
--drop view off_campus;

create or replace view on_campus_view as
SELECT 
    store_id,
    location,
    name,
    building_code,
    room_number,
    bevo_bucks
from store where location = 'on_campus';

create or replace TRIGGER on_campus_trigger
     INSTEAD OF insert ON on_campus_view
     FOR EACH ROW
BEGIN
    insert into Store(
        store_id,
        location,
        name,
        building_code,
        room_number,
        bevo_bucks)
    VALUES ( 
        :NEW.store_id,
        'on_campus',
        :NEW.name,
        :NEW.building_code,
        :NEW.room_number,
        :NEW.bevo_bucks) ;
END;
/

create or replace view off_campus_view as
SELECT 
    store_id,
    location,
    name,
    address1,
    address2,
    city,
    state,
    zip_code
FROM Store where location = 'Off_Campus';

create or replace TRIGGER off_campus_trigger
     INSTEAD OF insert ON off_campus_view
     FOR EACH ROW
BEGIN
    insert into Store(
        store_id,
        location,
        name,
        address1,
        address2,
        city,
        state,
        zip_code)
    VALUES ( 
        :NEW.store_id,
        'off_campus',
        :NEW.name,
        :NEW.address1,
        :NEW.address2,
        :NEW.city,
        :NEW.state,
        :NEW.zip_code) ;
END;
/